# time-logger

An example of using some of the features found on the STM32F429I microcontroller. This project targets the [STM32F429I-DISCO](http://www.st.com/en/evaluation-tools/32f429idiscovery.html) development board.

The initial project was generated using the [STM32CubeMX](http://www.st.com/en/development-tools/stm32cubemx.html) code generation tool and is developed using [Eclipse](http://www.eclipse.org/) with the [System Workbench for STM32](http://www.openstm32.org/System+Workbench+for+STM32) plugins.
